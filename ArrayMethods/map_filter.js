let arr = [{ key: 10, value: '1' }, { key: 20, value: '2' }, { key: 30, value: '3' }]

let reformattedArray = arr.map(
    function (obj) {
        let rObj = {}
        rObj.key = obj.value
        rObj.value = obj.key
        return rObj
    }
)

console.log(arr)
console.log(reformattedArray);

