class ToDo {
    constructor(title, currentId) {
        this.title = title;
        this.completed = false;
        this.id = currentId;
    }
}
class Service {
    constructor() {
        this.toDoArr = [];
        this.count = 1;
    }

    add(title) {
        this.toDoArr.push(new ToDo(title, this.count))
        this.count++;
    }


    remove(id) {
        let index = this.toDoArr.findIndex(function (task) { return task.id == id });
        console.log(index);
        if (index != -1) { this.toDoArr.splice(index, 1) }
    }

    completeToDo(id) {
        let index = this.toDoArr.findIndex(function (task) { return task.id == id });
        if (index != -1) {
            this.toDoArr[index].completed = true;
        }
    }

    completeAll() {
        if (this.toDoArr.every(function (todo) {
            todo.completed == true;
        })) {
            this.toDoArr.forEach(function (task) {
                task.completed = false;
            })
        }
        else {
            this.toDoArr.forEach(function (todo) {
                if (todo.completed) {
                    todo.completed = false;
                }
                else todo.completed = true;
            })
        }
    }

    edit(id, newTitle) {
        let index = this.toDoArr.findIndex(function (task) { return task.id == id });
        if (index != -1) { this.toDoArr[index] = new ToDo(newTitle) }
    }

    clearCompleted() {
        let indices = []
        this.toDoArr.forEach(function (task, index, array) {
            if (task.completed) indices.push(index)
        })
        let arr = this.toDoArr
        indices.forEach(function (i) { arr.splice(i, 1) })
    }

    showToDos(condition) {
        let ans = this.toDoArr.filter(function (task) { return condition(task) })
        ans.forEach(function (e) { console.log(e) });
    }
}

let s = new Service()
s.add('newTask1')
s.add('newTask2')
s.add('newTask3')
s.add('newTask4')

// s.remove(1)

s.completeAll()
s.clearCompleted();
console.log('All Tasks:: ');

s.showToDos(function (task) { return true })
// s.showToDos(function (task) { return task.completed == true })

console.log("All Incomplete task::")
s.showToDos(function (task) { return task.completed == false })