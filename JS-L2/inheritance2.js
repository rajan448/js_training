class A {
    constructor() {

    }
    m1() {

    }
}

class B extends A {
    constructor() {
        super();
    }
    m2() {

    }
}

console.log(A.prototype.hasOwnProperty('m1'));


