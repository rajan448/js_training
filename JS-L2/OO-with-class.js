
// let p1 = Person('Nag', 33) //Wrong
// let p2 = Person('Ria', 3)


// ES-6 Version
class Person {
    // let name // Cannot declare here
    constructor(name, age) { //keyword to create constructor 
        this.name = name
        this.age = age
        console.log('Person :: Constructor');

    }

    sayName() {  //Automatically inherited from prototype
        console.log(this.name);
    }
    sayAge() {
        console.log(this.age);
    }
}

// let p1 = new Person('Nag', 33)
// let p2 = new Person('Ria', 3)

class Employee extends Person {
    constructor(name, age, salary) {
        super(name, age)
        this.salary = salary
        console.log('Employee::Constructor');

    }

    saySalary() {
        console.log('i get ' + this.salary + ' only.');
    }
}

new Employee('Raj', 22, 1000).sayName();


class Boss extends Employee {

}

//---------------------------Static Memners of a class------------------------------

class Abc {
    static staticMethod() {
        //...
    }
}
Abc.staticVariables = 123;