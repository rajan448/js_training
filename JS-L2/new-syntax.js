// String interpolation (from ES-6)

// Object -literal enhancements

// let name = 'Nag';
// let age = 33;

// let person = {                  // ES 5
//     name: name,
//     age: age,

//     'home-adress': 'chennai',   //Fixed props only
//     sayName: function () { }
// }

// //or

// let dynamicField = 'home';      //home | office | vacation

// let personEs6 = {               // if prop name and ref name are same just use prop names
//     name,
//     age,
//     [dynamicField + '-address']: 'chennai',
//     sayName() {

//     },

//     ['my Method']() { }         // method names can have space in identifier
// }

// personEs6['my Method']()


// Array Destructuring

let salary = [100, 200, 300];
// let min = salary[0]
// let avg = salary[1]
// let max = salary[2]

//instead

let [min, avg, max, v = 400] = salary;

// console.log(min, max);
// console.log(v); //prints the default value of v

// Object destructuring

let person = {
    name: 'nag',
    age: 32
}

// let name = person.name
// let age = person.age

let { name, age } = person

// console.log(name);
// console.log(age);

// let { name: myName, age: myAge } = person

// console.log(myName, myAge);
let myName, myAge;
({ name: myName, age: myAge } = person)  // Error without paranthesis, Runtime considers it as an object


//---------------------spread operator------------------------------------

function func(a, b, c, d) {
    console.log(a);
    console.log(b);
    console.log(c);
    console.log(d);
}

let arr = [1, 2, 3, 4]

func(...arr)    // spreading this array to params

arr2 = [5, 6, 7]

let str = 'someString'
let ans = [...arr, 9, 0, ...arr2, ...str]

console.log(ans);

