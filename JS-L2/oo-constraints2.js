
let person = {
    _name: 'Nag',   //Data Properties   
    _age: 32,

    set name(name) {        //Accessor Properties
        if (name) {
            this._name = name
        }
        else {
            console.log('invalid name');

        }
    },

    get name() {
        return this._name
    },

    set age(age) {
        if (age) {
            this._age = age
        }
        else {
            console.log('Invalid age');
        }
    },

    get age(age) {
        return this._age;
    }
}
person.name = ""; // write /set
console.log(person.name)  // Read / get