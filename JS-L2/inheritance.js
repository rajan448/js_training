

function Person(name, age) {
    this.name = name;
    this.age = age;
}

Person.prototype.sayAge = function () {
    console.log('im ' + this.age);

}
Person.prototype.sayName = function () {
    console.log('im ' + this.name);

}

let p1 = new Person("Nag", 33)
let p2 = new Person('Ria', 3)

p1.sayName();
p1.sayAge();

console.log(p1.hasOwnProperty('sayName'));

//---------------------------------------------------------------