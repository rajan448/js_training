'use strict'

let person = {
    name: 'Nag',
    age: 32
};

Object.defineProperty(person, 'name', { configurable: false, writable: false, enumerable: false })

// Object.defineProperty(person, 'name', { configurable: true })  //cannot redifine property exception

//delete person.name; //No error in non-strict mode(default mode)
//person.name = 'raj'   // Read only property cannot change its value

for (let p in person) { //Name property is not enumerable here
    console.log(p);
    console.log(person[p]);
}

// Object.defineProperties() //for more than one properties
person.newProp = 'bla bla'

Object.preventExtensions(person)   // Prevents new properties addition to persons

console.log(Object.isExtensible(person));

Object.seal(person)                 // Prevents new Property addition ans deletion as well
Object.freeze(person)               // Prevents new Property addition, deletion and modification of all properties
                                    //Freezed objects are constants
