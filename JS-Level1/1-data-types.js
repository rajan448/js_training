//String
var name = "rajan";
var selection = 'abc';

// number
var count = 10;
var cost = 12.45;

// boolean
var isFound = true;

// undefined
var v;

//symbol (introduced in ES-6)


//----------------------------------------------------------------------
//Complex or reference types

var ref = new constructor(); //functions can act as a constructor
// functions having pascal case naming convention

//ES 5
function Employee() {

}

//ES-6
class Employee{
    constructor(){

    }
}

//By default .js objects are 
//   -extensible objects i.e. can add more props dynamically
//   -configurable (i.e. dynamically )

var config = new Object();

config.url = "http://";
config.method = "GET";
config.myProp = 'my_val';

config.success = function(){
    console.log('print result...');
};

delete config.myProp;


// - ---------Literal style object ---------------------
// #1
var newConfig = new Object(); 

var newConfig={
// Creating on the fly Object
//JSON Example
    url: 'http://', 
    method : 'GET', 
    myProp : 'my-value',
    success : function(){
        console.log('print result');
    }
};

// #2


var menu = new Array('veg', 'non-veg');
menu.push('biryani');

// or (literal style)

var menu = ['veg', 'non-veg'];

// #3

var regex = new RegExp('\\d{10}');
var ssn = new RegExp('\\d{3}-\\d{2}-\\d{4}');

//or literal syntax

var ssn2 = /\d{3}-\d{2}-\d{4}/;

// #4

var add = new Function();
var add = new Function('n1', 'n2', 'var r= n1+n2; return r;');

//or literal style

function add(n1, n2){
    var sum = n1+n2;
    return sum;
}


// Accessing the object properties


var person = {
    name : 'Nag',
};

person.name = "Rajan K";
person['name'] = 'rajan K';


console.log(person.log);
console.log(person['name']);

var person = {
    name : 'Nag',
    'home-address' : 'delhi'
};

//person.home-adress = 'chennai'; 
person['home-address'] = 'chennai';

//Use . notation if accessing a valid identifier property else use bracket notation

let propName = 'home-address';
console.log(person[propName]);