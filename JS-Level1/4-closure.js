/*

Why we need closures: 

    -> to abstract public behaviours of any module
    -> while executing functions async, to access parents scope data
*/



// #1

// function teach(sub) {
//     console.log("Teaching " + sub);
//     let notes = sub + " notes";     // available even after tech scope is closed
//     let funStuff = '............';  //destroyed after teach scope is closed
//     function learn() {
//         console.log('learning with ' + notes);
//     };
//     //learn()
//     console.log('teaching ends');
//     return learn;
// }
// let learnFunc = teach('.js');

// learnFunc();
// learnFunc();

// learnFunc = teach('.Java');
// learnFunc();



// #2
function init() {
    let count = 0;
    function doCount() {
        count += 1;
    }
    function getCount() {
        return count;
    }
    var o = {
        inc: doCount,
        get: getCount
    };
    return o;
}

let counter = init();
counter.inc();
counter.inc();
counter.inc();

console.log(counter.get());