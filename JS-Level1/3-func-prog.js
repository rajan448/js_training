/**
 * How to create function in JavaScript
 * 
 * 1. function declaration
 * 2. function expression
 */

//  add(23,34)
//  function add(n1, n2){
//      return n1+n2;
//  }

// console.log(add(2,4));


// --function obj is created at context creation and hoisted with function object.
// --named functions


// --------------------------------

// add(3,4); // error
// var add = function sum(n1,n2){return n1+n2; };  //<sum> name is optional and doesnt make any diference
// add(3,4);

// --anonymous function
// --created at context exec. phase, hoisted but undefined
// --can be executed after declaration only;

//-------------------------------------------------------------
// let isAdmin = true;
// function login()
// {
//     isAdmin = true;
// }
// login();
// let action; 

// if (isAdmin){
//     action = function(){console.log('Admin action');};
// }
// else{
//     action = function(){console.log('Guest action');};
// }

// action();


//-------------------------------------------------------------


//Function is an object

// function greet(f){
//     if(f)
//     {
//         f(); return;
//     }
//     console.log("Hello");
// }

// greet();
// greet(function(){console.log('ola');});

//-------------------------------------------------------------

// let nums = [2,5,3,4,8,6,7,1];

// nums.sort();

// nums.sort(function(a,b){
//     return a-b; 
// } );

// console.log(nums);


//-------------------------------------------------------------
//falsy value : 0, "", undefined, null, false, NAN
// remaining all are true value


//-------------------------------------------------------------

// function teach(){
//     console.log('teaching');

//     let learn= function(){console.log('learning.');};
//     learn();
// }
// teach();
//-------------------
// Returning function

// function teach(){
//     console.log('teaching');

//     let learn= function(){console.log('learning.');};
//     return learn;
// }
// let learnFunc = teach();
// learnFunc();

//------------------
// function makeAdder(n1){
//     return function(n2)
//     {
//         return n1+n2;
//     };

// }

// let add10 = makeAdder(10);
// console.log(add10(23));

//-------------

// function sum()
// {
//     console.dir(arguments);
//     let r=0;
//     len = arguments.length;
//     i=0;

//     while(i<len)
//     {
//         r+=arguments[i];
//         i++;
//     }
//     return r;
// }
// console.log(sum(1,2));
// console.log(sum(2,3,4));

//------------------------

//we cant differentiate btw function by its parameters

// function party(pay)
// {
//     if(arguments.length ===0)
//     {
//         return "No Party";
//     }
//     return "Sat Party ";
// }

// console.log(party());
// console.log( party(1000));

//-------------Function default params--

// function f1(a,b)
// {

//     // if(!a) a=1; //Default params
//     // if(!b) b=2; 

//     a = a||1;
//     b = b||4;

//     console.log(a);
//     console.log(b);
// }

// f1();

//--------------
// function f1(a=1,b=3)
// {
//     console.log(a);
//     console.log(b);
// }
// f1();

//----------------------

//functions with rest params

// function fun(a,b,...rest){
//     console.log(a);
//     console.log(b);

//     console.log(rest);
// }

// fun(1,2,3,4,5,6);


//--------------------------CLOSURES--------------------

