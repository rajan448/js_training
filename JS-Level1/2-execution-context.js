/*
Variable is hoisted to top with only default value

Global-context always executed by 'global-obj'

Global Object; 
    browser : window
    Node : process


Each function executed also creates a child context which is child of in-which context it has declared.

Reference Error / Type Error
*/


console.log(v);
v = 12;

function f1(){
    var v = 13; 
    console.log(v);
}

function f1(){
    console.log(v);
    var v = 13; 
}


var v = 100;

{
    var v =200; 
}


/*

Var Keyword: 

    always hoisted
    re-declaration
    no block scope
    we can mutate the reference of any variable

    solution: let & const keywords
*/

var tnr={
    name : 'Nag',
};

tnr = null;


console.log(v2);
let v2 = 10; 

//let v2 = 10; 

let v3 = 100; 

if(true)
    {
        let v3 = 200;
    }
console.log(v3); //100

var v4 = 100; 

if(true)
    {
        let v4 = 200;
    }
console.log(v4); // 100



// use of const

let trainer = {
    name : "Nag",
};

trainer = null; 

const trainer2 = {
    name : "Nag",
};

//trainer2 = null;  error
