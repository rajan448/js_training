let trainer = {
    name: 'Nag',
    doTeach: function () {
        console.log(this.name + ' teaching js ')
        let notes = '.js notes'
        let tnrName = this.name; //Get name of calling object

        //getting reference of calling object
        let self = this;

        let doLearn = function () {
            console.log(this.name + ' learning ' + notes + " from " + self.name)
        }
        //doLearn()
        let emp = { name: 'sapient' }
        doLearn.call(emp);
    }
}

trainer.doTeach()

let newTrainer = { name: 'Subu' }
trainer.doTeach.call(newTrainer)

let newTrainer2 = { name: 'Sam' }
trainer.doTeach.call(newTrainer2)

let newTrainer3 = {}
trainer.doTeach.call(newTrainer3)