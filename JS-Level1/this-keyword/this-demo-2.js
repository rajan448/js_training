//-------------------------Static function binding--------------------------------------------

// function sayNameForAll() {
//     //console.dir(this);
//     console.log('im ' + this.name);
// }


// let p1 = { name: 'Nag', sayName: sayNameForAll };
// let p2 = { name: 'Raj', sayName: sayNameForAll };

// sayNameForAll();    // function is bound to global object
// p1.sayName();
// p2.sayName();

//-------------------------Dynamic function binding--------------------------------------------

// let greetLib = {
//     sayName: function () {
//         console.log('im ' + this.name);
//     }
// };

let p = { name: 'Nag' };
let e = { name: 'Sapient' };

// //greetLib.sayName()    //Undefined

// greetLib.sayName.call(p)  // Invoking 'greetLib.sayName()' function by using p object
// greetLib.sayName.call(e)


let greetLib = {
    sayName: function (message, place) {

        console.log(arguments)
        console.log(message + ' im ' + this.name + ' from ' + place);
    }
};

// supply params

greetLib.sayName.call(p, 'Hi', 'Chennai')
greetLib.sayName.call(e, 'Hey', 'Delhi')

//Way -2 Dynamic method binding

greetLib.sayName.apply(p, ['Dude', 'Chennai', 'Delhi'])  // Passed arguments are in form of array

//Way -3 Dynamic method binding

let nagPersonSayName = greetLib.sayName.bind(p, 'HI', 'Bangalore', 'Kolkata'/* 3rd arg is of o use*/)
nagPersonSayName()

let sapientEmpSayName = greetLib.sayName.bind(e)
sapientEmpSayName("Hola ", "Mumbai");
