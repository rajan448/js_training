//constructors

// function Employee(name, age) {
//     this.name = name;
//     this.age = age;

//     this.sayName = function () {
//         console.log('im ' + this.name)
//     }
//     this.sayAge = function () {
//         console.log('im ' + this.age)
//     }
// }

// //Calling constructors
// let p1 = new Employee('Nag', 30); //constructor invocation this==> new-object
// let p2 = new Employee('Raj ', 22);

// p1.sayAge()
// p2.sayAge()

//----------------------Function Invocations-----------------------


function sessonStart() {

    function Employee(name) {
        this.name = name
    }

    function Trainer(name) {
        this.name = name

        this.doTeach = function () {
            console.log(this.name + ' teaching ')
            let notes = 'sub-notes'
            let self = this;

            let doLearn = function () {
                console.log(this.name + ' learning with ' + notes + ' from ' + self.name)
            }
            return doLearn
        }
    }

    let emp1 = new Employee('A');
    let emp2 = new Employee('B');

    let tnr = new Trainer('Nag');

    let tnr2 = new Trainer();   //Trainer Name Undefined
    let tnr3 = new Trainer('Sam')

    let doLearn = tnr.doTeach();

    doLearn.call(emp1);
    doLearn.call(emp2);

    tnr2.doTeach().call(emp1)
    tnr3.doTeach().call(emp2)
}
sessonStart();