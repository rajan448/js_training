function sessonStart() {

    function Employee(name) {
        this.name = name

        this.doLearn = function () {
            //console.log(this.name);
            console.log(this.name + ' teaching to ' + name)
        }
    }

    function Trainer(name) {
        this.name = name
    }

    let e1 = new Employee('A');
    let tnr = new Trainer('Nag');

    e1.doLearn.call(tnr)
}
sessonStart();